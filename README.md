# dwm-patches
My dwm patches for vanilla 6.1

## Patches so far:
- alsacontrols

  Volume control using native ALSA API, with X11 media key bindings.
- custombar

  This patch removes all of the status bar logic from dwm, and leaves on screen an adjustable gap for your bar.
- dynamic-gaps

  A fork of the [original gaps patch](https://dwm.suckless.org/patches/gaps/) from the suckless website, with on-the-fly gap size changing added.
- dynamic-uselessgap

  Same as above, but with the [uselessgap patch](https://dwm.suckless.org/patches/uselessgap/) as the base.
- mpdcontrol

  MPD playback control (play/pause, previous, next) using libmpdclient, with X11 media key bindings.
  
  (Also no, this is not stolen from the [suckless website](https://dwm.suckless.org/patches/mpdcontrol/). I haven't noticed that patch and did the (re)implementation myself. Use whichever patch you want.)
